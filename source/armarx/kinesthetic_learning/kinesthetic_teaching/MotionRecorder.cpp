/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "MotionRecorder.h"

#include <algorithm>
#include <cstddef>

#include <SimoxUtility/algorithm/get_map_keys_values.h>

#include "ArmarXCore/core/time/Clock.h"
#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/services/tasks/PeriodicTask.h>

#include <RobotAPI/libraries/armem_robot_state/client/common/RobotReader.h>

#include "EventChannel.h"
#include <armarx/kinesthetic_learning/kinesthetic_teaching/EventChannel.h>
#include <range/v3/algorithm/none_of.hpp>
#include <range/v3/all.hpp>
#include <range/v3/range/conversion.hpp>
#include <range/v3/view/reverse.hpp>

namespace armarx::kinesthetic_learning::kinesthetic_teaching
{

    MotionRecorder::MotionRecorder(const armarx::armem::robot::RobotDescription& description,
                                   armarx::armem::robot_state::RobotReader& robotReader,
                                   const Params& params) :
        description(description), robotReader(robotReader), params(params)
    {
    }

    MotionRecorder::~MotionRecorder()
    {
        if (task)
        {
            if (not stop())
            {
                ARMARX_WARNING << "Failed to stop motion recorder task";
            }
        }
    }

    void
    MotionRecorder::registerEventChannel(const EventChannel* event)
    {
        eventProviders.insert(event);
    }

    bool
    MotionRecorder::start()
    {
        if (task)
        {
            ARMARX_WARNING << "MotionRecorder already started";
            return false;
        }

        task = new armarx::PeriodicTask<MotionRecorder>(
            this, &MotionRecorder::runStep, params.recordingPeriodMs);
        task->start();

        return true;
    }

    bool
    MotionRecorder::stop()
    {
        if (not task)
        {
            ARMARX_WARNING << "MotionRecorder not started so it cannot be stopped";
            return false;
        }

        task->stop();
        task = nullptr;

        return true;
    }

    void
    MotionRecorder::runStep()
    {
        const auto isEnabled = [](const EventChannel::State& state) { return state.enabled; };

        const auto stati = eventProviders |
                           ranges::views::transform([](const EventChannel* eventChannel)
                                                    { return eventChannel->state(); }) |
                           ranges::to_vector;

        if (ranges::none_of(stati, isEnabled))
        {
            ARMARX_VERBOSE << deactivateSpam(1) << "All channels disabled. Won't record anything";
            return;
        }

        TimeStep timestep;

        for (const auto& [eventChannel, status] : ranges::views::zip(eventProviders, stati))
        {
            timestep.states[eventChannel->name()] = status;
        }

        const auto robotState = robotReader.queryState(description, Clock::Now());
        if (not robotState.has_value())
        {
            ARMARX_WARNING << "Could not fetch robot state";
            return;
        }

        timestep.robotState = robotState.value();

        // data must be new to be inserted
        if (not data.empty())
        {
            if (data.back().robotState.timestamp == timestep.robotState.timestamp)
            {
                ARMARX_WARNING << "No new data could be fetched from memory. Skipping";
                return;
            }
        }

        data.push_back(timestep);
    }

    const std::vector<MotionTimeStep>&
    MotionRecorder::recordedData() const
    {
        return data;
    }

    void
    MotionRecorder::report() const
    {
        ARMARX_INFO << "MotionRecorder::report";

        if (data.empty())
        {
            ARMARX_INFO << "no data";
            return;
        }

        const auto recordingDuration =
            data.back().robotState.timestamp - data.front().robotState.timestamp;

        // report overall duration
        ARMARX_INFO << "Length: " << (recordingDuration).toMilliSeconds() << "ms";


        // report start and end of individual arms
        for (const auto& name : simox::alg::get_keys(data.front().states))
        {
            const auto isActive = [&name](const TimeStep& step)
            { return step.states.at(name).enabled; };

            const std::size_t start = std::distance(data.begin(), ranges::find_if(data, isActive));

            const auto reversed = ranges::views::reverse(data);
            const std::size_t end =
                data.size() - 1 -
                std::distance(reversed.begin(), ranges::find_if(reversed, isActive));

            ARMARX_INFO << "[" << name << "] start: " << data.at(start).robotState.timestamp
                        << ", duration: "
                        << (data.at(end).robotState.timestamp - data.at(start).robotState.timestamp)
                               .toMilliSeconds()
                        << "ms";
        }

        // report samples
        ARMARX_INFO << "Number of samples: " << data.size() << " vs. expected: "
                    << recordingDuration.toMilliSeconds() / params.recordingPeriodMs;
    }


} // namespace armarx::kinesthetic_learning::kinesthetic_teaching
