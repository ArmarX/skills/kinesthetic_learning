/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ZeroTorqueController.h"

#include <VirtualRobot/RobotNodeSet.h>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/logging/Logging.h>


#include <RobotAPI/interface/units/RobotUnit/NjointZeroTorqueController.h>


namespace armarx::kinesthetic_learning::kinesthetic_teaching
{

    ZeroTorqueController::ZeroTorqueController(const VirtualRobot::RobotNodeSetPtr& rns,
                                               const armarx::RobotUnitInterfacePrx& robotUnit,
                                               const std::string& controllerPrefix) :
        controllerName(controllerPrefix + "ZeroTorqueCtrl" + rns->getName()), robotUnit(robotUnit)
    {
        ARMARX_CHECK_NOT_NULL(robotUnit);
        ARMARX_CHECK_NOT_NULL(rns);

        ARMARX_INFO << "Creating zero torque controller with name `" << controllerName << "`";
        createZeroTorqueController(rns);
    }

    void
    ZeroTorqueController::enable()
    {
        ARMARX_INFO << "Activating controller `" << controllerName << "`";
        robotUnit->activateNJointController(controllerName);
    }

    void
    ZeroTorqueController::disable()
    {
        ARMARX_INFO << "Disabling controller `" << controllerName << "`";
        robotUnit->deactivateNJointController(controllerName);
    }

    ZeroTorqueController::~ZeroTorqueController()
    {
        ARMARX_INFO << "Disabling and deleting controller `" << controllerName << "`";
        robotUnit->deactivateAndDeleteNJointController(controllerName);
    }

    void
    ZeroTorqueController::createZeroTorqueController(const VirtualRobot::RobotNodeSetPtr& rns)
    {
        armarx::NJointZeroTorqueControllerConfigPtr config(
            new armarx::NJointZeroTorqueControllerConfig());

        config->maxTorque = 0;

        for (auto& name : rns->getNodeNames())
        {
            config->jointNames.push_back(name);
        }

        ARMARX_CHECK(not config->jointNames.empty())
            << "The robot node set does not provide any nodes!";

        auto ctrlPrx = robotUnit->getNJointController(controllerName);
        if (!ctrlPrx)
        {
            ARMARX_INFO << "Creating controller `" << controllerName << "`";
            ctrlPrx = robotUnit->createNJointController(
                armarx::RobotUnitControllerNames::NJointZeroTorqueController,
                controllerName,
                config);
        }
    }


} // namespace armarx::kinesthetic_learning::kinesthetic_teaching
