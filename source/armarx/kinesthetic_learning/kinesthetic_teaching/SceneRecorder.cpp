/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "SceneRecorder.h"

#include <algorithm>
#include <cstddef>

#include "ArmarXCore/core/time/Clock.h"
#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/services/tasks/PeriodicTask.h>


namespace armarx::kinesthetic_learning::kinesthetic_teaching
{
    SceneRecorder::SceneRecorder(const armarx::objpose::ObjectPoseClient& objectPoseClient,
                                 const Params& params) :
        objectPoseClient(objectPoseClient), params(params)
    {
    }

    SceneRecorder::~SceneRecorder()
    {
        if (task)
        {
            if (not stop())
            {
                ARMARX_WARNING << "Failed to stop motion recorder task";
            }
        }
    }

    bool
    SceneRecorder::start()
    {
        if (task)
        {
            ARMARX_WARNING << "ObjectPoseRecorder already started";
            return false;
        }

        task = new armarx::PeriodicTask<SceneRecorder>(
            this, &SceneRecorder::runStep, params.recordingPeriodMs);
        task->start();

        return true;
    }

    bool
    SceneRecorder::stop()
    {
        if (not task)
        {
            ARMARX_WARNING << "ObjectPoseRecorder not started so it cannot be stopped";
            return false;
        }

        task->stop();
        task = nullptr;

        return true;
    }

    void
    SceneRecorder::runStep()
    {
        TimeStep step{.timestamp = Clock::Now(), .sceneByProvider = {}};

        for (const auto& providerName :
             objectPoseClient.getObjectPoseStorage()->getAvailableProviderNames())
        {
            step.sceneByProvider.emplace(
                providerName, objectPoseClient.fetchObjectPosesFromProvider(providerName));
        }

        data.push_back(step);
    }

    const std::vector<SceneTimeStep>&
    SceneRecorder::recordedData() const
    {
        return data;
    }

    void
    SceneRecorder::report() const
    {
        ARMARX_INFO << "ObjectPoseRecorder::report";

        if (data.empty())
        {
            ARMARX_INFO << "no data";
            return;
        }

        const auto recordingDuration = data.back().timestamp - data.front().timestamp;

        // report overall duration
        ARMARX_INFO << "Length: " << (recordingDuration).toMilliSeconds() << "ms";

        // report samples
        ARMARX_INFO << "Number of samples: " << data.size() << " vs. expected: "
                    << recordingDuration.toMilliSeconds() / params.recordingPeriodMs;
    }


} // namespace armarx::kinesthetic_learning::kinesthetic_teaching
