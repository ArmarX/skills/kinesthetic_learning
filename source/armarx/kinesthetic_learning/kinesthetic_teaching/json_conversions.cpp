#include "json_conversions.h"

#include <RobotAPI/libraries/ArmarXObjects/json_conversions.h>

#include <SimoxUtility/json/eigen_conversion.h>
#include "ArmarXCore/core/time/Duration.h"

#include "armarx/kinesthetic_learning/kinesthetic_teaching/EventChannel.h"
#include "armarx/kinesthetic_learning/kinesthetic_teaching/MotionRecorder.h"

#include <RobotAPI/libraries/RobotStatechartHelpers/RobotNameHelper.h>
#include <VirtualRobot/XML/RobotIO.h>

#include <fstream>

namespace armarx::armem::robot
{

    void
    from_json(const nlohmann::json& j, RobotState& bo)
    {
        bo.globalPose = j["globalPose"];
        bo.jointMap = j["jointMap"].get<decltype(bo.jointMap)>();
        bo.timestamp = armarx::Duration::MicroSeconds(j["timestamp"].get<int>());
    }

    void
    to_json(nlohmann::json& j, const RobotState& bo)
    {
        j["globalPose"] = bo.globalPose;
        j["jointMap"] = bo.jointMap;
        j["timestamp"] = bo.timestamp.toMicroSecondsSinceEpoch();
    }
} // namespace armarx::armem::robot

namespace armarx::kinesthetic_learning::kinesthetic_teaching
{

    void
    from_json(const nlohmann::json& j, EventChannel::State& bo)
    {
        bo.enabled = j["enabled"];
    }

    void
    to_json(nlohmann::json& j, const EventChannel::State& bo)
    {
        j["enabled"] = bo.enabled;
    }

    void
    from_json(const nlohmann::json& j, MotionTimeStep& bo)
    {
        bo.robotState = j["robotState"];
        bo.states = j["states"].get<decltype(bo.states)>();
    }


    void
    to_json(nlohmann::json& j, const MotionTimeStep& bo)
    {
        j["robotState"] = bo.robotState;
        j["states"] = bo.states;
    }


    void
    from_json(const nlohmann::json& j, SceneTimeStep& bo)
    {
        bo.timestamp = Duration::MicroSeconds(j["timestamp"]);
        bo.sceneByProvider = j["sceneByProvider"].get<decltype(bo.sceneByProvider)>();
    }


    void
    to_json(nlohmann::json& j, const SceneTimeStep& bo)
    {
        j["timestamp"] = bo.timestamp.toMicroSecondsSinceEpoch();
        j["sceneByProvider"] = bo.sceneByProvider;
    }

    std::map<double, std::vector<double> > getTrajDataFromJson(std::string filepath, std::string side)
    {
        std::map<double, std::vector<double>> trajData;
        nlohmann::json kinestheticTeachingData;


        {
            // attention: currently hardcoded to this data path
            std::string armarxDataPath = "armar6_skills/bimanual_kinesthetic_teaching/" + filepath;

            ArmarXDataPath::getAbsolutePath(armarxDataPath, filepath);
            std::ifstream ifs{filepath};
            ifs >> kinestheticTeachingData;
            if (kinestheticTeachingData.empty())
            {
                ARMARX_ERROR << "No content in:" << VAROUT(filepath);
            }
        }

        // attention: currently hardcoded to armar 6
        std::string robotName;
        ArmarXDataPath::getAbsolutePath(
                    "armar6_rt/robotmodel/Armar6-SH/Armar6-SH.xml",
                    robotName);
        VirtualRobot::RobotPtr robot = VirtualRobot::RobotIO::loadRobot(robotName);

        RobotNameHelperPtr rnh = RobotNameHelper::Create(robotName);
        armarx::core::time::DateTime firstTimeStep;
        bool firstTimestepSet = false;
        // read json
        if (kinestheticTeachingData.find("motion") != kinestheticTeachingData.end())
        {
            auto robotStates = kinestheticTeachingData["motion"];

            for (const auto& item: robotStates)
            {
                auto robotStateJson = item.at("robotState");
                armarx::armem::robot::RobotState robotState;
                armarx::armem::robot::from_json(robotStateJson, robotState);
                if(!firstTimestepSet){
                    firstTimeStep = robotState.timestamp;
                    firstTimestepSet = true;
                }
                armarx::core::time::Duration duration = robotState.timestamp - firstTimeStep;
                auto timestep = duration.toMicroSeconds();
                std::map<std::string, float> jointMap = robotState.jointMap;
                robot->setJointValues(jointMap);

                // right trajectory

                std::string sidekey = side;
                sidekey[0] = toupper(sidekey[0]);
                RobotNameHelper::RobotArm arm = rnh->getRobotArm(sidekey, robot);

                VirtualRobot::RobotNodePtr tcp = arm.getTCP();
                Eigen::Matrix4f poseTCP = tcp->getPoseInRootFrame();

                std::vector<double> poseVec;
                for (size_t i = 0; i < 3; ++i)
                {
                    poseVec.push_back(poseTCP.coeff(i,3));
                }
                VirtualRobot::MathTools::Quaternion quat = VirtualRobot::MathTools::eigen4f2quat(poseTCP);
                poseVec.push_back(quat.w);
                poseVec.push_back(quat.x);
                poseVec.push_back(quat.y);
                poseVec.push_back(quat.z);
                trajData.insert(std::make_pair(timestep, poseVec));
            }
        }

        return trajData;
    }


} // namespace armarx::kinesthetic_learning::kinesthetic_teaching
