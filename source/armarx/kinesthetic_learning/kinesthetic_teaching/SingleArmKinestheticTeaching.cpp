/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "SingleArmKinestheticTeaching.h"

#include <ArmarXCore/core/logging/Logging.h>

#include "armarx/kinesthetic_learning/kinesthetic_teaching/trigger/TriggerInterface.h"
#include <armarx/kinesthetic_learning/kinesthetic_teaching/ZeroTorqueController.h>


namespace armarx::kinesthetic_learning::kinesthetic_teaching
{
    SingleArmKinestheticTeaching::SingleArmKinestheticTeaching(
        trigger::TriggerInterface& trigger,
        ZeroTorqueControllerInterface& zeroTorqueController,
        const std::string& name) :
        trigger(trigger), zeroTorqueController(zeroTorqueController), kinName(name)
    {
        setTag("SingleArmKinestheticTeaching_" + kinName);
    }

    void
    SingleArmKinestheticTeaching::run()
    {
        ARMARX_IMPORTANT << "Waiting for force spike";
        trigger.waitForEvent();

        // init procedure
        ARMARX_IMPORTANT << "Force spike detected. Enabling zero torque mode";
        zeroTorqueController.enable();

        // let motion recorder know that this arm is enabled
        kinState.enabled = true;

        // run kin teaching until the FT sensor is pushed again
        ARMARX_IMPORTANT << "Now, move the arm around.";
        trigger.waitForEvent();

        ARMARX_IMPORTANT << "Force spike detected. Disabling zero torque mode";
        kinState.enabled = false;
        zeroTorqueController.disable();

        // done
        stop();
        ARMARX_IMPORTANT << "Done.";
    }

    SingleArmKinestheticTeaching::~SingleArmKinestheticTeaching()
    {
        stop();
    }

    void
    SingleArmKinestheticTeaching::stop()
    {
        kinState.enabled = false;
        zeroTorqueController.disable();
    }

    const std::string&
    SingleArmKinestheticTeaching::name() const
    {
        return kinName;
    }

    SingleArmKinestheticTeaching::State
    SingleArmKinestheticTeaching::state() const
    {
        return kinState;
    }

} // namespace armarx::kinesthetic_learning::kinesthetic_teaching
