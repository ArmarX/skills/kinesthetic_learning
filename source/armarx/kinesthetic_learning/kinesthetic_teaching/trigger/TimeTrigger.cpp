#include "TimeTrigger.h"

#include <chrono>
#include <thread>

#include "ArmarXCore/core/logging/Logging.h"

namespace armarx::kinesthetic_learning::kinesthetic_teaching::trigger
{
    TimeTrigger::TimeTrigger(const int waitMs) : waitMs(waitMs)
    {
    }

    void
    TimeTrigger::waitForEvent()
    {
        ARMARX_INFO << "Event will be emitted in " << waitMs << "ms";
        std::this_thread::sleep_for(std::chrono::milliseconds(waitMs));
    }


} // namespace armarx::kinesthetic_learning::kinesthetic_teaching::trigger
