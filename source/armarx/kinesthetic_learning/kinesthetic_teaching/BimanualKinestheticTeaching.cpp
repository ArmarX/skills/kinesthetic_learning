/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "BimanualKinestheticTeaching.h"

namespace armarx::kinesthetic_learning::kinesthetic_teaching
{

    BimanualKinestheticTeaching::BimanualKinestheticTeaching(SingleArmKinestheticTeaching& left,
                                                             SingleArmKinestheticTeaching& right,
                                                             MotionRecorder& motionRecorder,
                                                             SceneRecorder& sceneRecorder) :
        leftArmTeaching(left),
        rightArmTeaching(right),
        motionRecorder(motionRecorder),
        sceneRecorder(sceneRecorder)
    {
    }

    BimanualKinestheticTeaching::~BimanualKinestheticTeaching()
    {
        if (leftKinTeachingTask)
        {
            leftKinTeachingTask->stop();
        }

        if (rightKinTeachingTask)
        {
            rightKinTeachingTask->stop();
        }
    }


    void
    BimanualKinestheticTeaching::run()
    {
        // setup
        leftKinTeachingTask = new armarx::SimpleRunningTask([&]() { leftArmTeaching.run(); },
                                                            "LeftArmKinestheticTeaching");
        rightKinTeachingTask = new armarx::SimpleRunningTask([&]() { rightArmTeaching.run(); },
                                                             "RightArmKinestheticTeaching");

        motionRecorder.registerEventChannel(&leftArmTeaching);
        motionRecorder.registerEventChannel(&rightArmTeaching);

        // start tasks
        leftKinTeachingTask->start();
        rightKinTeachingTask->start();

        // start motion recording
        if (not motionRecorder.start())
        {
            ARMARX_ERROR << "Failed to start motion recorder";
            return;
        }

        // start scene recorder
        if(not sceneRecorder.start())
        {
            ARMARX_ERROR << "Failed to start scene recorder";
            return;
        }

        // wait until kinestetic teaching for both arms is finished
        leftKinTeachingTask->waitForFinished();
        rightKinTeachingTask->waitForFinished();

        // finish / stop recording
        if(not motionRecorder.stop())
        {
            ARMARX_WARNING << "Failed to stop motion recorder!";
        }

        if(not sceneRecorder.stop())
        {
            ARMARX_WARNING << "Failed to stop scene recorder!";
        }
    }

} // namespace armarx::kinesthetic_learning::kinesthetic_teaching
