/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <vector>

#include <ArmarXCore/core/services/tasks/PeriodicTask.h>

#include <RobotAPI/libraries/armem_robot/types.h>

#include <armarx/kinesthetic_learning/kinesthetic_teaching/EventChannel.h>


namespace armarx::armem::robot_state
{
    class RobotReader;
}

namespace armarx::kinesthetic_learning::kinesthetic_teaching
{

    class EventChannel;

    struct MotionTimeStep
    {
        std::unordered_map<std::string, EventChannel::State> states;
        armarx::armem::robot::RobotState robotState;
    };


    struct MotionRecorderParams
    {
        int recordingPeriodMs = 10;
    };


    class MotionRecorder
    {
    public:
        using Params = MotionRecorderParams;
        using TimeStep = MotionTimeStep;


        MotionRecorder(const armarx::armem::robot::RobotDescription& description,
                       armarx::armem::robot_state::RobotReader& robotReader,
                       const Params& params = Params());

        MotionRecorder(MotionRecorder&) = delete;

        ~MotionRecorder();

        void registerEventChannel(const EventChannel* event);

        [[nodiscard]] bool start();
        [[nodiscard]] bool stop();


        const std::vector<MotionTimeStep>& recordedData() const;

        void report() const;


    protected:
    private:
        void runStep();

        const armarx::armem::robot::RobotDescription description;
        armarx::armem::robot_state::RobotReader& robotReader;

        const Params params;

        std::set<const EventChannel*> eventProviders;
        std::vector<MotionTimeStep> data;

        armarx::PeriodicTask<MotionRecorder>::pointer_type task;
    };
  
    
} // namespace armarx::kinesthetic_learning::kinesthetic_teaching
