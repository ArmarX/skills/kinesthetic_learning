/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/core/logging/Logging.h>

#include <armarx/kinesthetic_learning/kinesthetic_teaching/EventChannel.h>

namespace armarx::kinesthetic_learning::kinesthetic_teaching::trigger
{
    class TriggerInterface;

}

namespace armarx::kinesthetic_learning::kinesthetic_teaching
{

    class ZeroTorqueControllerInterface;

    class SingleArmKinestheticTeaching : virtual public EventChannel, virtual public armarx::Logging
    {
    public:
        SingleArmKinestheticTeaching(trigger::TriggerInterface& trigger,
                                     ZeroTorqueControllerInterface& zeroTorqueController,
                                     const std::string& name);

        SingleArmKinestheticTeaching(SingleArmKinestheticTeaching&) = delete;
        SingleArmKinestheticTeaching(SingleArmKinestheticTeaching&&) = delete;

        ~SingleArmKinestheticTeaching() override;

        void run();

        const std::string& name() const override;
        State state() const override;

    protected:
        void stop();

    private:
        void waitForForceSpike();

        trigger::TriggerInterface& trigger;
        ZeroTorqueControllerInterface& zeroTorqueController;

        const std::string kinName;

        EventChannel::State kinState{.enabled = false};
    };


} // namespace armarx::kinesthetic_learning::kinesthetic_teaching
