/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <vector>

#include <ArmarXCore/core/services/tasks/PeriodicTask.h>

#include "RobotAPI/libraries/ArmarXObjects/ObjectPoseClient.h"
#include <RobotAPI/libraries/armem_robot/types.h>

#include <armarx/kinesthetic_learning/kinesthetic_teaching/EventChannel.h>


namespace armarx::kinesthetic_learning::kinesthetic_teaching
{

    struct SceneTimeStep
    {
        armarx::DateTime timestamp;
        std::map<std::string, armarx::objpose::ObjectPoseSeq> sceneByProvider;
    };


    struct ObjectPoseRecorderParams
    {
        int recordingPeriodMs = 100;
    };


    class SceneRecorder
    {
    public:
        using Params = ObjectPoseRecorderParams;
        using TimeStep = SceneTimeStep;

        SceneRecorder(const armarx::objpose::ObjectPoseClient& objectPoseClient,
                           const Params& params = Params());

        SceneRecorder(SceneRecorder&) = delete;

        ~SceneRecorder();

        [[nodiscard]] bool start();
        [[nodiscard]] bool stop();


        const std::vector<SceneTimeStep>& recordedData() const;

        void report() const;

    protected:
    private:
        void runStep();

        armarx::objpose::ObjectPoseClient objectPoseClient;
        const Params params;

        std::vector<SceneTimeStep> data;

        armarx::PeriodicTask<SceneRecorder>::pointer_type task;
    };


} // namespace armarx::kinesthetic_learning::kinesthetic_teaching
