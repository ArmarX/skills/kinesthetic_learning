/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/core/services/tasks/TaskUtil.h>

#include <RobotAPI/interface/units/RobotUnit/RobotUnitInterface.h>

#include "armarx/kinesthetic_learning/kinesthetic_teaching/SceneRecorder.h"
#include <armarx/kinesthetic_learning/kinesthetic_teaching/MotionRecorder.h>
#include <armarx/kinesthetic_learning/kinesthetic_teaching/SingleArmKinestheticTeaching.h>

namespace armarx::kinesthetic_learning::kinesthetic_teaching
{

    class BimanualKinestheticTeaching
    {
    public:
        BimanualKinestheticTeaching(SingleArmKinestheticTeaching& left,
                                    SingleArmKinestheticTeaching& right,
                                    MotionRecorder& motionRecorder,
                                    SceneRecorder& sceneRecorder);
                                    
        BimanualKinestheticTeaching(BimanualKinestheticTeaching&) = delete;

        void run();


        ~BimanualKinestheticTeaching();

    protected:
    private:
        SingleArmKinestheticTeaching& leftArmTeaching;
        SingleArmKinestheticTeaching& rightArmTeaching;
        MotionRecorder& motionRecorder;
        SceneRecorder& sceneRecorder;

        armarx::SimpleRunningTask<>::pointer_type leftKinTeachingTask;
        armarx::SimpleRunningTask<>::pointer_type rightKinTeachingTask;
    };


} // namespace armarx::kinesthetic_learning::kinesthetic_teaching
