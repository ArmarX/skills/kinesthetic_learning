/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/interface/units/ForceTorqueUnit.h>

namespace armarx::kinesthetic_learning::kinesthetic_teaching
{

   


    struct FTWristSensorParams
    {
        float forceThreshold = 20.F;

        int windowSizeMs = 200;
        int cycleTimeMs = 10;

        Eigen::Vector3f forceDetectionMask = Eigen::Vector3f::Ones();
    };

    class FTWristSensor
    {
    public:
        using Params = FTWristSensorParams;


        FTWristSensor(const armarx::ForceTorqueUnitObserverInterfacePrx& forceTorqueObserver,
                      const std::string& ftDatafieldName,
                      const Params& params = Params());

        FTWristSensor(FTWristSensor&) = delete;

        enum class Edge
        {
            RISING,
            FALLING
        };

        void waitForForceSpike(Edge edge) const;

    private:
        armarx::ForceTorqueUnitObserverInterfacePrx forceTorqueObserver;

        const std::string ftDatafieldName;
        const Params params;
    };


} // namespace armarx::kinesthetic_learning::kinesthetic_teaching
