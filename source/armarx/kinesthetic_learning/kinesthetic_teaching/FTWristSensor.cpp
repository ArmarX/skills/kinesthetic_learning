/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "FTWristSensor.h"

#include <ArmarXCore/observers/Observer.h>
#include <ArmarXCore/observers/variant/DatafieldRef.h>

#include <RobotAPI/libraries/core/FramedPose.h>

namespace armarx::kinesthetic_learning::kinesthetic_teaching
{

    FTWristSensor::FTWristSensor(
        const armarx::ForceTorqueUnitObserverInterfacePrx& forceTorqueObserver,
        const std::string& ftDatafieldName,
        const Params& params) :
        forceTorqueObserver(forceTorqueObserver), ftDatafieldName(ftDatafieldName), params(params)
    {
    }

    void
    FTWristSensor::waitForForceSpike(const Edge edge) const
    {
        ARMARX_VERBOSE << "Waiting for force spike";

        armarx::DatafieldRefPtr forceDf = armarx::DatafieldRefPtr::dynamicCast(
            forceTorqueObserver->getForceDatafield(ftDatafieldName));

        ARMARX_CHECK_NOT_NULL(forceDf);

        auto getForce = [&]() -> float
        {
            return forceDf->getDataField()
                ->get<armarx::FramedDirection>()
                ->toEigen()
                .cwiseProduct(params.forceDetectionMask)
                .norm();
        };

        std::deque<float> spikes(params.windowSizeMs / params.cycleTimeMs, getForce());

        while (true) // stop run function if returning true
        {
            const float force = getForce();
            ARMARX_VERBOSE << VAROUT(force);

            spikes.push_back(force);
            spikes.pop_front();

            float refValue = spikes.at(0);
            bool low = true;
            bool risingEdgeDetected = false;
            bool fallingEdgeDetected = false;

            bool f2rDetected = false;
            bool r2fDetected = false;

            for (const float spike : spikes)
            {
                if (low)
                {
                    if (spike < refValue)
                    {
                        refValue = spike;
                    }
                    else if (spike > refValue + params.forceThreshold)
                    {
                        low = false;
                        risingEdgeDetected = true;
                        f2rDetected |= fallingEdgeDetected;
                    }
                }

                if (!low)
                {
                    if (spike > refValue)
                    {
                        refValue = spike;
                    }
                    else if (spike < refValue - params.forceThreshold)
                    {
                        low = true;
                        fallingEdgeDetected = true;
                        r2fDetected |= risingEdgeDetected;
                    }
                }
            }

            if(f2rDetected)
            {
                ARMARX_VERBOSE << "Rising edge detected";
            }

            if(r2fDetected)
            {
                ARMARX_VERBOSE << "Falling edge detected";
            }

            switch (edge)
            {
                case Edge::RISING:
                {
                    if (f2rDetected)
                    {
                        return;
                    }
                    break;
                }
                case Edge::FALLING:
                {
                    if (r2fDetected)
                    {
                        return;
                    }
                    break;
                }
            }


            std::this_thread::sleep_for(std::chrono::milliseconds{params.cycleTimeMs});
        }
    }
} // namespace armarx::kinesthetic_learning::kinesthetic_teaching
