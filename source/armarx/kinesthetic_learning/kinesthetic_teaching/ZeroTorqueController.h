/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <VirtualRobot/VirtualRobot.h>

#include <RobotAPI/interface/units/RobotUnit/RobotUnitInterface.h>

namespace armarx::kinesthetic_learning::kinesthetic_teaching
{

    class ZeroTorqueControllerInterface
    {
    public:
        virtual void enable() = 0;
        virtual void disable() = 0;

        virtual ~ZeroTorqueControllerInterface() = default;
    };


    class ZeroTorqueController : virtual public ZeroTorqueControllerInterface
    {
    public:
        ZeroTorqueController(const VirtualRobot::RobotNodeSetPtr& rns,
                             const armarx::RobotUnitInterfacePrx& robotUnit,
                             const std::string& controllerPrefix = "KinestheticTeaching");

        ZeroTorqueController(ZeroTorqueController&) = delete;

        void enable() override;
        void disable() override;

        ~ZeroTorqueController() override;


    private:
        void createZeroTorqueController(const VirtualRobot::RobotNodeSetPtr& rns);

        const std::string controllerName;
        armarx::RobotUnitInterfacePrx robotUnit;
    };


} // namespace armarx::kinesthetic_learning::kinesthetic_teaching
