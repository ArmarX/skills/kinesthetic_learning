/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <SimoxUtility/json/json.hpp>

#include <armarx/kinesthetic_learning/kinesthetic_teaching/MotionRecorder.h>
#include <armarx/kinesthetic_learning/kinesthetic_teaching/SceneRecorder.h>

namespace armarx::armem::robot
{

    void from_json(const nlohmann::json& j, RobotState& bo);
    void to_json(nlohmann::json& j, const RobotState& bo);
}
namespace armarx::kinesthetic_learning::kinesthetic_teaching
{

    void from_json(const nlohmann::json& j, MotionTimeStep& bo);
    void to_json(nlohmann::json& j, const MotionTimeStep& bo);

    void from_json(const nlohmann::json& j, SceneTimeStep& bo);
    void to_json(nlohmann::json& j, const SceneTimeStep& bo);

    std::map<double, std::vector<double>> getTrajDataFromJson(std::string filepath, std::string side_rns);
} // namespace armarx::kinesthetic_learning::kinesthetic_teaching
