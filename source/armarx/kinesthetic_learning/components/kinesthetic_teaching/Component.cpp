/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx::kinesthetic_learning::ArmarXObjects::kinesthetic_teaching
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "Component.h"

#include <filesystem>
#include <fstream>
#include <iomanip>
#include <mutex>
#include <string>

#include <SimoxUtility/json/json.hpp>
#include <VirtualRobot/Robot.h>

#include <ArmarXCore/core/PackagePath.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/services/tasks/TaskUtil.h>
#include <ArmarXCore/libraries/DecoupledSingleComponent/Decoupled.h>

#include "RobotAPI/libraries/ArmarXObjects/forward_declarations.h"
#include <RobotAPI/libraries/armem_robot/types.h>

#include "armarx/kinesthetic_learning/kinesthetic_teaching/debug/ZeroTorqueControllerDummy.h"
#include "armarx/kinesthetic_learning/kinesthetic_teaching/trigger/FTWristSensorTrigger.h"
#include "armarx/kinesthetic_learning/kinesthetic_teaching/trigger/TimeTrigger.h"
#include <armarx/kinesthetic_learning/kinesthetic_teaching/BimanualKinestheticTeaching.h>
#include <armarx/kinesthetic_learning/kinesthetic_teaching/FTWristSensor.h>
#include <armarx/kinesthetic_learning/kinesthetic_teaching/MotionRecorder.h>
#include <armarx/kinesthetic_learning/kinesthetic_teaching/SingleArmKinestheticTeaching.h>
#include <armarx/kinesthetic_learning/kinesthetic_teaching/ZeroTorqueController.h>
#include <armarx/kinesthetic_learning/kinesthetic_teaching/json_conversions.h>

namespace armarx::kinesthetic_learning::components::kinesthetic_teaching
{

    using namespace armarx::kinesthetic_learning::kinesthetic_teaching;

    const std::string Component::defaultName = "kinesthetic_teaching";


    Component::Component() : virtualRobotReader(memoryNameSystem())
    {
        // addPlugin(robotUnitPlugin);
    }

    armarx::PropertyDefinitionsPtr
    Component::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr def =
            new armarx::ComponentPropertyDefinitions(getConfigIdentifier());

        // Publish to a topic (passing the TopicListenerPrx).
        // def->topic(myTopicListener);

        // Subscribe to a topic (passing the topic name).
        // def->topic<PlatformUnitListener>("MyTopic");

        // Use (and depend on) another component (passing the ComponentInterfacePrx).
        def->component(forceTorqueUnitObserver, "Armar6ForceTorqueObserver");

        // Add a required property. (The component won't start without a value being set.)
        // def->required(properties.boxLayerName, "p.box.LayerName", "Name of the box layer in ArViz.");

        // Add an optional property.
        def->optional(properties.robot.name, "p.robot.name", "e.g. `Armar6`");
        def->optional(properties.robot.rns.leftArm, "p.robot.rns.leftArm", "");
        def->optional(properties.robot.rns.rightArm, "p.robot.rns.rightArm", "");

        def->optional(properties.ft.leftFtDataField, "ft.leftFtDataField");
        def->optional(properties.ft.rightFtDataField, "ft.rightFtDataField");

        virtualRobotReader.registerPropertyDefinitions(def);

        return def;
    }


    void
    Component::onInitComponent()
    {
        // Topics and properties defined above are automagically registered.

        // Keep debug observer data until calling `sendDebugObserverBatch()`.
        // (Requies the armarx::DebugObserverComponentPluginUser.)
        // setDebugObserverBatchModeEnabled(true);

        char* home = getenv("HOME");
        ARMARX_CHECK_NOT_NULL(home);

        logDir = std::filesystem::path(std::string(home)) / "kinesthetic_teaching";
    }


    void
    Component::onConnectComponent()
    {
        // Do things after connecting to topics and components.

        /* (Requies the armarx::DebugObserverComponentPluginUser.)
        // Use the debug observer to log data over time.
        // The data can be viewed in the ObserverView and the LivePlotter.
        // (Before starting any threads, we don't need to lock mutexes.)
        {
            setDebugObserverDatafield("numBoxes", properties.numBoxes);
            setDebugObserverDatafield("boxLayerName", properties.boxLayerName);
            sendDebugObserverBatch();
        }
        */

        /* (Requires the armarx::ArVizComponentPluginUser.)
        // Draw boxes in ArViz.
        // (Before starting any threads, we don't need to lock mutexes.)
        drawBoxes(properties, arviz);
        */

        // Setup the remote GUI.
        {
            createRemoteGuiTab();
            RemoteGui_startRunningTask();
        }

        virtualRobotReader.connect();

        robot = virtualRobotReader.getRobot(properties.robot.name);
    }

    class DebugReporter
    {
    };

    void
    Component::run()
    {
        ARMARX_CHECK_NOT_NULL(forceTorqueUnitObserver);
        ARMARX_CHECK_NOT_NULL(getRobotUnit());
        ARMARX_CHECK_NOT_NULL(robot);

        const armarx::armem::robot::RobotDescription robotDescription{.name = robot->getName()};

        MotionRecorder motionRecorder(robotDescription, virtualRobotReader);
        SceneRecorder sceneRecorder(armarx::ObjectPoseClientPluginUser::getClient());

        FTWristSensor leftWristSensor(forceTorqueUnitObserver, properties.ft.leftFtDataField);
        FTWristSensor rightWristSensor(forceTorqueUnitObserver, properties.ft.rightFtDataField);

        trigger::FTWristSensorTrigger leftTrigger(leftWristSensor);
        trigger::FTWristSensorTrigger rightTrigger(rightWristSensor);

        ZeroTorqueController leftArmZeroTorqueController(
            robot->getRobotNodeSet(properties.robot.rns.leftArm),
            /*robotUnitPlugin->*/ getRobotUnit());
        ZeroTorqueController rightArmZeroTorqueController(
            robot->getRobotNodeSet(properties.robot.rns.rightArm),
            /*robotUnitPlugin->*/ getRobotUnit());

        SingleArmKinestheticTeaching leftArmKinTeaching(
            leftTrigger, leftArmZeroTorqueController, properties.robot.rns.leftArm);
        SingleArmKinestheticTeaching rightArmKinTeaching(
            rightTrigger, rightArmZeroTorqueController, properties.robot.rns.rightArm);

        BimanualKinestheticTeaching kinestheticTeaching(
            leftArmKinTeaching, rightArmKinTeaching, motionRecorder, sceneRecorder);

        ARMARX_IMPORTANT << "You can now use the FT sensor in the wrist to start the recording.";
        kinestheticTeaching.run();

        motionRecorder.report();
        sceneRecorder.report();

        saveData(motionRecorder.recordedData(), sceneRecorder.recordedData());
    }

    void
    Component::runInSimulation()
    {
        ARMARX_CHECK_NOT_NULL(forceTorqueUnitObserver);
        ARMARX_CHECK_NOT_NULL(getRobotUnit());
        ARMARX_CHECK_NOT_NULL(robot);

        const armarx::armem::robot::RobotDescription robotDescription{.name = robot->getName()};

        MotionRecorder motionRecorder(robotDescription, virtualRobotReader);
        SceneRecorder sceneRecorder(armarx::ObjectPoseClientPluginUser::getClient());

        trigger::TimeTrigger leftTrigger(5000);
        trigger::TimeTrigger rightTrigger(4000);

        debug::ZeroTorqueControllerDummy leftArmZeroTorqueController;
        debug::ZeroTorqueControllerDummy rightArmZeroTorqueController;

        SingleArmKinestheticTeaching leftArmKinTeaching(
            leftTrigger, leftArmZeroTorqueController, properties.robot.rns.leftArm);
        SingleArmKinestheticTeaching rightArmKinTeaching(
            rightTrigger, rightArmZeroTorqueController, properties.robot.rns.rightArm);

        BimanualKinestheticTeaching kinestheticTeaching(
            leftArmKinTeaching, rightArmKinTeaching, motionRecorder, sceneRecorder);

        ARMARX_IMPORTANT << "Running in simulation. Will be automated.";
        kinestheticTeaching.run();

        motionRecorder.report();
        sceneRecorder.report();

        saveData(motionRecorder.recordedData(), sceneRecorder.recordedData());
    }

    void
    Component::saveData(const std::vector<MotionTimeStep>& motionTimeSeries,
                        const std::vector<SceneTimeStep>& sceneTimeSeries)
    {
        // to json

        nlohmann::json j;

        auto& jMeta = j["meta"];
        jMeta["robot"]["name"] = properties.robot.name;
        jMeta["robot"]["rns"]["left"] = properties.robot.rns.leftArm;
        jMeta["robot"]["rns"]["right"] = properties.robot.rns.rightArm;

        jMeta["annotation"] = tab.meta.annotation.getValue();

        j["motion"] = motionTimeSeries;
        j["scene"] = sceneTimeSeries;


        // save to disk

        const auto p1 = std::chrono::system_clock::now();

        const std::string timestamp = std::to_string(
            std::chrono::duration_cast<std::chrono::seconds>(p1.time_since_epoch()).count());

        const auto jsonFilename =
            logDir / (timestamp + "-" + tab.meta.annotation.getValue() + ".json");

        ARMARX_IMPORTANT << "Saving to file `" << jsonFilename << "`";

        if (not std::filesystem::exists(logDir))
        {
            ARMARX_INFO << "Creating directory `" << logDir << "`";
            std::filesystem::create_directories(logDir);
        }

        std::ofstream ofs(jsonFilename);
        ofs << std::setw(4) << j;

        ARMARX_INFO << "Done.";
    }


    void
    Component::onDisconnectComponent()
    {
        runTask->stop();
        runTask = nullptr;
    }


    void
    Component::onExitComponent()
    {
    }


    std::string
    Component::getDefaultName() const
    {
        return Component::defaultName;
    }


    std::string
    Component::GetDefaultName()
    {
        return Component::defaultName;
    }

    void
    Component::startKinestheticTeaching(const ::Ice::Current& /*unused*/)
    {
        ARMARX_INFO << "Received kinesthetic teaching request.";

        // kinesthetic teaching can only be started once
        std::lock_guard<std::mutex> g{kinTeachingMtx};

        if (runTask)
        {
            ARMARX_IMPORTANT
                << "Kinesthetic teaching is already active. Your request will not be handled!";
            return;
        }

        ARMARX_IMPORTANT << "Starting kinesthetic teaching.";


        if (getRobotUnit()->isSimulation())
        {
            runTask = new armarx::SimpleRunningTask<>(
                [&]() { runInSimulation(); }, "KinestheticTeachingTaskComponentInSimulation");
        }
        else
        {
            runTask = new armarx::SimpleRunningTask<>([&]() { run(); },
                                                      "KinestheticTeachingTaskComponent");
        }

        runTask->start();
    }

    void
    Component::stopKinestheticTeaching(const ::Ice::Current& /*unused*/)
    {
        if (not runTask)
        {
            ARMARX_INFO << "Kinesthetic teaching has not yet been started. Cannot be stopped.";
            return;
        }

        runTask->stop();
        runTask = nullptr;
    }


    void
    Component::createRemoteGuiTab()
    {
        using namespace armarx::RemoteGui::Client;

        // Setup the widgets.
        tab.control.startButton.setLabel("START");
        tab.control.stopButton.setLabel("STOP");

        tab.meta.label.setText("Text");
        tab.meta.annotation.setValue("");

        tab.status.left.label.setText("Left arm");
        tab.status.left.status.setText("...");

        tab.status.right.label.setText("Right arm");
        tab.status.right.status.setText("...");


        // Setup the layout.

        GridLayout gridAnnotation;
        gridAnnotation.add(tab.meta.label, {0, 0});
        gridAnnotation.add(tab.meta.annotation, {0, 1});

        GridLayout gridControl;
        gridControl.add(tab.control.startButton, {0, 0});
        gridControl.add(tab.control.stopButton, {0, 1});

        GridLayout gridStatus;
        gridStatus.add(tab.status.left.label, {0, 0});
        gridStatus.add(tab.status.left.status, {0, 1});
        gridStatus.add(tab.status.right.label, {1, 0});
        gridStatus.add(tab.status.right.status, {1, 1});

        VBoxLayout root = {gridAnnotation, VSpacer(), gridControl, VSpacer(), gridStatus};
        RemoteGui_createTab(getName(), root, &tab);
    }


    void
    Component::RemoteGui_update()
    {

        if (tab.control.startButton.wasClicked())
        {
            startKinestheticTeaching();
        }

        if (tab.control.stopButton.wasClicked())
        {
            stopKinestheticTeaching();
        }


        // if (tab.boxLayerName.hasValueChanged() || tab.numBoxes.hasValueChanged())
        // {
        //     std::scoped_lock lock(propertiesMutex);
        //     properties.boxLayerName = tab.boxLayerName.getValue();
        //     properties.numBoxes = tab.numBoxes.getValue();

        //     {
        //         setDebugObserverDatafield("numBoxes", properties.numBoxes);
        //         setDebugObserverDatafield("boxLayerName", properties.boxLayerName);
        //         sendDebugObserverBatch();
        //     }
        // }
        // if (tab.drawBoxes.wasClicked())
        // {
        //     // Lock shared variables in methods running in seperate threads
        //     // and pass them to functions. This way, the called functions do
        //     // not need to think about locking.
        //     std::scoped_lock lock(propertiesMutex, arvizMutex);
        //     drawBoxes(properties, arviz);
        // }
    }


    /* (Requires the armarx::ArVizComponentPluginUser.)
    void
    Component::drawBoxes(const Component::Properties& p, viz::Client& arviz)
    {
        // Draw something in ArViz (requires the armarx::ArVizComponentPluginUser.
        // See the ArVizExample in RobotAPI for more examples.

        viz::Layer layer = arviz.layer(p.boxLayerName);
        for (int i = 0; i < p.numBoxes; ++i)
        {
            layer.add(viz::Box("box_" + std::to_string(i))
                      .position(Eigen::Vector3f(i * 100, 0, 0))
                      .size(20).color(simox::Color::blue()));
        }
        arviz.commit(layer);
    }
    */


    ARMARX_REGISTER_COMPONENT_EXECUTABLE(Component, Component::GetDefaultName());

} // namespace armarx::kinesthetic_learning::components::kinesthetic_teaching
