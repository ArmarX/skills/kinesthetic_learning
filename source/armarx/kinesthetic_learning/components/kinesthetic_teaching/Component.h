/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx::kinesthetic_learning::ArmarXObjects::kinesthetic_teaching
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


#include <string>

#include <VirtualRobot/VirtualRobot.h>

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/services/tasks/TaskUtil.h>

#include <ArmarXGui/libraries/ArmarXGuiComponentPlugins/LightweightRemoteGuiComponentPlugin.h>
#include <ArmarXGui/libraries/RemoteGui/Client/Widgets.h>

#include <RobotAPI/components/units/ForceTorqueObserver.h>
#include <RobotAPI/libraries/ArmarXObjects/plugins/ObjectPoseClientPlugin.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/RobotUnitComponentPlugin.h>
#include <RobotAPI/libraries/armem/client.h>
#include <RobotAPI/libraries/armem_robot_state/client/common/VirtualRobotReader.h>

#include <armarx/kinesthetic_learning/components/kinesthetic_teaching/ComponentInterface.h>
#include <armarx/kinesthetic_learning/kinesthetic_teaching/MotionRecorder.h>
#include <armarx/kinesthetic_learning/kinesthetic_teaching/SceneRecorder.h>


namespace armarx::kinesthetic_learning::components::kinesthetic_teaching
{

    class Component :
        virtual public armarx::Component,
        virtual public armarx::kinesthetic_learning::components::kinesthetic_teaching::
            ComponentInterface,
        virtual public armarx::LightweightRemoteGuiComponentPluginUser,
        virtual armarx::ObjectPoseClientPluginUser,
        // , virtual public armarx::ArVizComponentPluginUser
        virtual public armarx::armem::client::ComponentPluginUser,
        virtual public armarx::RobotUnitComponentPluginUser

    {
    public:
        Component();
        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;

        /// Get the component's default name.
        static std::string GetDefaultName();


        void startKinestheticTeaching(const ::Ice::Current& = ::Ice::emptyCurrent) override;
        void stopKinestheticTeaching(const ::Ice::Current& = ::Ice::emptyCurrent) override;


    protected:
        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        /// @see armarx::ManagedIceObject::onInitComponent()
        void onInitComponent() override;

        /// @see armarx::ManagedIceObject::onConnectComponent()
        void onConnectComponent() override;

        /// @see armarx::ManagedIceObject::onDisconnectComponent()
        void onDisconnectComponent() override;

        /// @see armarx::ManagedIceObject::onExitComponent()
        void onExitComponent() override;


        /// This function should be called once in onConnect() or when you
        /// need to re-create the Remote GUI tab.
        void createRemoteGuiTab();

        /// After calling `RemoteGui_startRunningTask`, this function is
        /// called periodically in a separate thread. If you update variables,
        /// make sure to synchronize access to them.
        void RemoteGui_update() override;


    private:
        // Private methods go here.

        void run();
        void runInSimulation();

        // Forward declare `Properties` if you used it before its defined.
        // struct Properties;

        /* (Requires the armarx::ArVizComponentPluginUser.)
        /// Draw some boxes in ArViz.
        void drawBoxes(const Properties& p, viz::Client& arviz);
        */

        void saveData(
            const std::vector<::armarx::kinesthetic_learning::kinesthetic_teaching::MotionTimeStep>&
                motionTimeSeries,
            const std::vector<::armarx::kinesthetic_learning::kinesthetic_teaching::SceneTimeStep>&
                sceneTimeSeries);


    private:
        static const std::string defaultName;


        // Private member variables go here.
        VirtualRobot::RobotPtr robot;
        armarx::armem::robot_state::VirtualRobotReader virtualRobotReader;

        armarx::SimpleRunningTask<>::pointer_type runTask;

        std::filesystem::path logDir;

        std::mutex kinTeachingMtx;


        // plugins
        // armarx::plugins::RobotUnitComponentPlugin* robotUnitPlugin;

        // proxies
        armarx::ForceTorqueUnitObserverInterfacePrx forceTorqueUnitObserver;


        /// Properties shown in the Scenario GUI.
        struct Properties
        {
            struct
            {
                std::string name = "Armar6";

                struct
                {
                    std::string leftArm = "LeftArm";
                    std::string rightArm = "RightArm";
                } rns;

            } robot;


            struct
            {
                std::string leftFtDataField = "FT L_ArmL_FT";
                std::string rightFtDataField = "FT R_ArmR_FT";
            } ft;
        };
        Properties properties;
        /* Use a mutex if you access variables from different threads
         * (e.g. ice functions and RemoteGui_update()).
        std::mutex propertiesMutex;
        */


        /// Tab shown in the Remote GUI.
        struct RemoteGuiTab : armarx::RemoteGui::Client::Tab
        {
            struct
            {
                armarx::RemoteGui::Client::Button startButton;
                armarx::RemoteGui::Client::Button stopButton;
            } control;

            struct
            {
                armarx::RemoteGui::Client::Label label;
                armarx::RemoteGui::Client::LineEdit annotation;

            } meta;

            struct
            {
                struct Arm
                {
                    armarx::RemoteGui::Client::Label label;
                    armarx::RemoteGui::Client::Label status;
                };

                Arm left;
                Arm right;

            } status;
        };
        RemoteGuiTab tab;


        /* (Requires the armarx::ArVizComponentPluginUser.)
         * When used from different threads, an ArViz client needs to be synchronized.
        /// Protects the arviz client inherited from the ArViz plugin.
        std::mutex arvizMutex;
        */
    };


} // namespace armarx::kinesthetic_learning::components::kinesthetic_teaching
