# Kinesthetic Learning

The purpose of this package is to equip the robot with the following skills

- learning via kinesthetic teaching
- learning from its own experience

Both paradigms can be used to generate joint space and task space trajectories. The robot can subsequently learn to generalize and reproduce the demonstrations (e.g. through motion primitives) and refine its behavior.
